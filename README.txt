Automatic transformation of the URL in comment using Google Shortener API

INSTALLATION

 1. Download and enable this module.

 2. Done! The module will automatically reduce long links in comments
 on your site.
